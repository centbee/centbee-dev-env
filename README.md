# Centbee Dev Env image

### Pull newest build from Docker Hub
```
docker pull centbee/dev-env:latest
```

### Run image
```
docker run -it centbee/dev-env:latest bash
```

### Use as base image
```Dockerfile
FROM centbee/dev-env:latest
```
