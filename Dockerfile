FROM ubuntu:bionic

LABEL maintainer="lorien@centbee.com" \
	org.label-schema.schema-version="1.0" \
	org.label-schema.name="centbee/dev-env" \
	org.label-schema.description="Android Dev Env Image" \
	org.label-schema.vendor="Lorien Gamaroff (Centbee)" \
	org.label-schema.license="MIT" 

RUN apt-get update -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN apt-get install software-properties-common -y

RUN add-apt-repository ppa:openjdk-r/ppa -y
RUN apt-get update -y
RUN apt-get install openjdk-8-jdk -y && java -version

RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64

RUN apt-get install android-sdk -y








